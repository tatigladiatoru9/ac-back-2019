$(document).ready(function () {
    $('select').material_select();
});

document.querySelector('#regbutton').addEventListener("click", (e) => {
    e.preventDefault();
    toastr.remove()
    let error = false;
    const rezervare = {
        nume: document.querySelector('#nume').value,
        prenume: document.querySelector('#prenume').value,
        email: document.querySelector('#email').value,
        telefon: document.querySelector('#telefon').value,
        numeFilm: document.querySelector('#numeFilm').value,
        data: document.querySelector('#data').value,
        ora: document.querySelector('#ora').value,
        nrBilete: document.querySelector("#nrBilete").value,
        rand: document.querySelector('#rand').value,
        loc: document.querySelector('#loc').value
    }

    if ((parseInt(rezervare.rand)) === NaN) {
        error = true;
        toastr.error("Campul permite doar cifre!");
    } else if ((parseInt(rezervare.nrBilete)) === "NaN") {
        error = true;
        toastr.error("Campul permite doar cifre!");
    } else if ((parseInt(rezervare.loc)) === "NaN") {
        error = true;
        toastr.error("Campul permite doar cifre!");
    }

    if (!error) {
        axios.post('/bilet', rezervare)
            .then((response) => {
                toastr.success("Rezervare adaugata!");
            })
            .catch((error) => {
                const values = Object.values(error.response.data)
                console.log(error);
                values.map(item => {
                    toastr.error(item)
                })
            })
    }
}, false)